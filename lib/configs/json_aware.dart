abstract class JsonAware {
  Map<String, dynamic> toJson();

  static bool readOptionBool(Map<String, dynamic> map, String optName, bool defValue) {
    if(!map.containsKey(optName))
      return defValue;

    String value = map[optName] as String;
    value = value.toLowerCase();

    if(value == "true" || value == "yes" || value == "on")
      return true;

    return false;
  }

  static Map<String, dynamic> writeOptionBool(String optName, bool value) {
    if(value) {
      return { optName: "true" };
    } else {
      return { optName: "false" };
    }
  }
}