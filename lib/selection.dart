import 'package:cafe_app/either_one_of.dart';

class Selection {
  int _id;
  int _fid; // FestivalId
  int _sid; // ScoreId
  int _rid; // TownshipId
  int _flag;

  Selection(this._id, this._rid, this._fid, this._sid, this._flag);

  Selection.map(dynamic object) {
    this._id = object['id'];
    this._rid = eitherOneOf(object, ['rid', 'townshipId']);
    this._fid = eitherOneOf(object, ['fid', 'festivalId']);
    this._sid = eitherOneOf(object, ['sid', 'scoreId']);

    // Dynamic typing is always awesome!
    if(object['flag'] == 'true' || object['flag'] == '1' || object['flag'] == true || object['flag'] == 1) {
      this._flag = 1;
    } else if(object['flag'] == 'false' || object['flag'] == '0' || object['flag'] == null || object['flag'] == false || object['flag'] == 0) {
      this._flag = 0;
    }
  }

  int get id => this._id;
  int get festivalId => this._fid;
  int get townshipId => this._rid;
  int get flag => this._flag;
  int get scoreId => this._sid;

  Map<String, dynamic> toMap() {
    var m = Map<String, dynamic>();
    m['id'] = this.id;
    m['fid'] = this.festivalId;
    m['sid'] = this.scoreId;
    m['rid'] = this.townshipId;
    m['flag'] = this.flag;
    return m;
  }

  Map<String, dynamic> toMapNoId() {
    var m = this.toMap();
    m.remove('id');
    return m;
  }

  @override
  String toString() {
    return "Selection[id=${this.id}, townshipId=${this.townshipId}, festivalId=${this.festivalId}, scoreId=${this.scoreId}, flag=${this.flag}";
  }
}
