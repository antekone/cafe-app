import 'package:cafe_app/loading_page.dart';
import 'package:cafe_app/score.dart';
import 'package:flutter/cupertino.dart';
import 'package:cafe_app/township.dart';
import 'package:cafe_app/festival.dart';
import 'package:cafe_app/database_helper.dart';
import 'package:cafe_app/edit_festival_page.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/property_list_view.dart';

class FestivalEditionError implements Exception {
  final String cause;
  FestivalEditionError(this.cause);
}

class AddFestivalPage extends StatefulWidget {
  final Township restaurant;
  final Festival festival;
  final String prevPageTitle;

  // If 'festival' is specified, this widget will enter 'edit' mode instead of
  // 'add' mode.
  AddFestivalPage({this.restaurant, this.prevPageTitle, this.festival});

  @override
  _AddFestivalPageState createState() =>
    _AddFestivalPageState();
}

class _AddFestivalPageState extends State<AddFestivalPage> {
  DateTime _selectedDate = DateTime.now();
  int _yellow = 0, _red = 0, _blue = 0, _tasks = 0;

  // "ff" in this name originally meant "fifty-fifty", but it doesn't have
  // much sense anymore. Rename it?
  bool _ffModeFlag = false;

  // Fields used in edit mode (up to '---').
  bool _userPointsCalculated = false;
  int _sumOfTrophies = 0;
  int _sumOfTasks = 0;
  bool _trophyPointsError = false;
  bool _taskPointsError = false;
  bool _diamondPointsError = false;
  bool _allowUpdate = true;
  // ---

  _AddFestivalPageState();

  @override
  void initState() {
    super.initState();

    if(this._isEditMode()) {
      this._yellow = this.f.yellow;
      this._red = this.f.red;
      this._blue = this.f.blue;
      this._tasks = this.f.tasks;
      this._selectedDate = this.f.dateTime;
      this._ffModeFlag = this._hasTasks();
      this._countUserPointsForEditValidation().then((_) {
        Future.delayed(Duration(milliseconds: 250)).then((_) {
          setState(() {
            _userPointsCalculated = true;
          });
        });
      });
    }
  }

  Township get r => this.widget.restaurant;
  Festival get f => this.widget.festival;

  bool _isEditMode() => this.f != null;
  bool _hasTasks() => this.f.tasks != null && this.f.tasks > 0;

  Future<void> _countUserPointsForEditValidation() async {
    var db = DatabaseHelper();
    List<Score> scoreList = await db.getScoreListByFestivalId(this.f.id);

    this._sumOfTasks = scoreList.fold(0, (sum, el) => sum += el.tasks);
    this._sumOfTrophies = scoreList.fold(0, (sum, el) => sum += el.yellow);
  }

  void _recalculateErrorFlags() {
    if(this._isEditMode()) {
      this._trophyPointsError = this._yellow < this._sumOfTrophies;
      this._taskPointsError = this._tasks < this._sumOfTasks;
      this._diamondPointsError = (this._blue == 0 && this._red == 0);
      this._allowUpdate = !(this._trophyPointsError || this._taskPointsError || this._diamondPointsError);
    } else {
      this._trophyPointsError = false;
      this._taskPointsError = false;
      this._diamondPointsError = false;
      this._allowUpdate = true;
    }
  }

  void _addOrModifyFestival() {
    if(this._isEditMode()) {
      _editFestival();
    } else {
      _addFestival();
    }
  }

  Future<void> _editFestivalWorker(Festival newF) async {
    var db = DatabaseHelper();

    // Question for later: is it better to issue multiple ALTER items instead
    // of removing old festival and then adding new festival?
    //
    // Issuing multiple ALTER items could be handled in the Festival object
    // itself instead of here, to cut down the number of places which depend
    // on the internal state of the Festival.

    await db.rawRemoveFestivalById(newF.id);
    await db.addFestivalRetainingId(newF);

    // OK.
  }

  void _editFestival() {
    var id = this.f.id;
    var date = this._selectedDate.millisecondsSinceEpoch;
    var newF = Festival(id, r.id, "", date, this._yellow, this._red,
      this._blue, this._tasks, this.widget.festival.method);

    this._editFestivalWorker(newF).then((_) {
      Navigator.pop(this.context);
    }).catchError((e) {
      // Error updating! Should we handle it?
    }, test: (e) => e is FestivalEditionError);
  }

  void _addFestival() {
    var db = DatabaseHelper();
    var f = Festival(0, r.id, "", this._selectedDate.millisecondsSinceEpoch,
      this._yellow, this._red, this._blue, this._tasks, Festival.DEFAULT_METHOD);

    db.addFestival(f).then((int id) {
      var editFestivalRoute = CupertinoPageRoute(builder: (context) =>
        EditFestivalPage(fid: id));

      Navigator.push(context, editFestivalRoute).then((result) {
        Navigator.of(context).pop();
      });
    });
  }

  bool _validateForm() {
    var flag = this._selectedDate != null && this._yellow > 0 && (this._red > 0 || this._blue > 0);

    if(this._ffModeFlag) {
      if(this._tasks == 0)
        return false;
    }

    return flag;
  }

  Widget _buildIndicatorOrPage() {
    if(this._isEditMode() && !this._userPointsCalculated) {
      return LoadingPage(withScaffold: false);
    } else {
      return this._buildPage();
    }
  }

  Widget _buildPage() {
    String questEditBodySuffix = "";
    String trophyEditBodySuffix = "";

    if(this._isEditMode()) {
      final String minStr = i18n(this.context).editDescSuffixMinimum;

      trophyEditBodySuffix = "\n\n$minStr: ${this._sumOfTrophies} ${i18n(this.context).globTrophySuffix(this._sumOfTrophies)}.";
      questEditBodySuffix = "\n\n$minStr: ${this._sumOfTasks} ${i18n(this.context).globQuestSuffix(this._sumOfTasks)}.";
    }

    return PropertyListView(
      items: [
        !this._isEditMode() ? LinePropertyWidget.from(i18n(this.context).festChoose) : null,
        !this._isEditMode() ? BooleanPropertyWidget(name: i18n(this.context).festUseQuests, defaultValue: this._ffModeFlag,
          setCallback: (bool v) => setState(() {
            this._ffModeFlag = v;
            this._recalculateErrorFlags();
          }),
          getCallback: () => this._ffModeFlag) : null,

        !this._isEditMode() ? LinePropertyWidget.from(i18n(this.context).festChooseProperties) : null,

        DatePropertyWidget.from(i18n(this.context).festDate, _selectedDate,
          setCallback: (newDate) => setState(() {
            print("tz=${newDate.timeZoneName}");
            this._selectedDate = newDate;
            this._recalculateErrorFlags();
          })),

        NumericPropertyWidget.from(i18n(this.context).globTrophyPoints, this._yellow, suffix: i18n(this.context).globPointSuffix,
          editTitle: i18n(this.context).festTrophyCount,
          editBody: i18n(this.context).festTrophiesReceivedTitle + trophyEditBodySuffix,
          erroneousValue: this._trophyPointsError,
          setCallback: (value) => setState(() {
            this._yellow = value;
            this._recalculateErrorFlags();
          })),

        this._trophyPointsError ?
          LinePropertyWidget.from(i18n(this.context).editRemoveSomePlayersFirst, right: true, color: CupertinoColors.destructiveRed) :
          null,

        this._ffModeFlag ?
          NumericPropertyWidget.from(i18n(this.context).festQuests, this._tasks, suffix: i18n(this.context).globQuestSuffix,
            editTitle: i18n(this.context).festQuestCount,
            editBody: i18n(this.context).festQuestReceivedTitle + questEditBodySuffix,
            erroneousValue: this._taskPointsError,
            setCallback: (value) => setState(() {
              this._tasks = value;
              this._recalculateErrorFlags();
            })) :
          null,

        this._taskPointsError ?
          LinePropertyWidget.from(i18n(this.context).editRemoveSomePlayersFirst, right: true, color: CupertinoColors.destructiveRed) :
          null,

        NumericPropertyWidget.from(i18n(this.context).festDiamonds, this._blue, suffix: i18n(this.context).globDiamondSuffix,
          editTitle: i18n(this.context).festDiamondCount,
          editBody: i18n(this.context).festDiamondReceivedTitle,
          erroneousValue: this._diamondPointsError,
          setCallback: (value) => setState(() {
            this._blue = value;
            this._recalculateErrorFlags();
          })),

        NumericPropertyWidget.from(i18n(this.context).festRubies, this._red, suffix: i18n(this.context).globRubySuffix,
          editTitle: i18n(this.context).festRubyCount,
          editBody: i18n(this.context).festRubyReceivedTitle,
          erroneousValue: this._diamondPointsError,
          setCallback: (value) => setState(() {
            this._red = value;
            this._recalculateErrorFlags();
          })),
      ]
    );
  }

  @override
  Widget build(BuildContext context) {
    final String addButtonTitle = this._isEditMode()
      ? i18n(this.context).globUpdate
      : i18n(this.context).globCreate;

    final String pageTitle = this._isEditMode()
        ? i18n(this.context).festEditPageTitle
        : i18n(this.context).mainNewFestival;

    var _addButton = CupertinoButton(
        padding: EdgeInsets.all(0),
        child: Text(addButtonTitle),
        onPressed: this._allowUpdate ? (this._validateForm() ? _addOrModifyFestival : null) : null);

    var _navBar = CupertinoNavigationBar(
      middle: Text(pageTitle),
      previousPageTitle: this.widget.prevPageTitle == null ? this.widget.restaurant.name : this.widget.prevPageTitle,
      trailing: _addButton,
    );

    return CupertinoPageScaffold(
      navigationBar: _navBar,
      child: _buildIndicatorOrPage(),
    );
  }
}
