import 'dart:core';

const Map<String, String> version = 
{
  "frameworkVersion": "1.3.9",
  "channel": "unknown",
  "repositoryUrl": "unknown source",
  "frameworkRevision": "f91df4abe1427fef8862c9e81b2e5af6fc05a67a",
  "frameworkCommitDate": "2019-03-09 21:19:28 -0500",
  "engineRevision": "4e54bc93ca9aaf2156fb06266c9fe509e8599a5f",
  "dartSdkVersion": "2.2.1 (build 2.2.1-dev.1.0 2fb6cd9f5f)"
}
;
