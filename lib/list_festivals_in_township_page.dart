import 'package:cafe_app/utils/page_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/festival.dart';
import 'package:cafe_app/database_helper.dart';
import 'package:cafe_app/edit_festival_page.dart';
import 'package:cafe_app/dismiss.dart';
import 'package:cafe_app/add_festival_page.dart';
import 'package:cafe_app/township.dart';
import 'package:cafe_app/property_list_view.dart';
import 'package:cafe_app/ads.dart' as Ads;

class ListFestivalsInTownshipPage extends StatefulWidget {
  final int rid;
  ListFestivalsInTownshipPage({this.rid});

  @override
  _ListFestivalsInTownshipPageState createState() =>
    _ListFestivalsInTownshipPageState(rid);
}

class _ListFestivalsInTownshipPageState extends State<ListFestivalsInTownshipPage> {
  var items = List<Festival>();
  final int rid;
  Township r;
  var df = DateFormat("d MMMM yyyy (EEEE)");

  _ListFestivalsInTownshipPageState(this.rid);

  Future<void> _loadData() async {
    var items = await DatabaseHelper().loadFestivalsForTownshipId(this.rid);
    var r = await DatabaseHelper().getTownshipById(this.rid);

    setState(() {
      this.items = items;
      this.r = r;
    });
  }

  @override
  void initState() {
    super.initState();
    this._loadData();
  }

  void _editFestivalDataPage(BuildContext context, Festival f) {
    PageUtils.push(context, AddFestivalPage(
        restaurant: this.r,
        festival: f,
        prevPageTitle: i18n(context).mainFestivals
    )).then((_) {
      this._loadData();
    });
  }

  void _selectFestival(Festival f) {
    var editFestivalRoute = CupertinoPageRoute(builder: (context) =>
      EditFestivalPage(fid: f.id));

    Navigator.push(context, editFestivalRoute);
  }

  void _addNewFestival() {
    DatabaseHelper().getTownshipById(this.rid).then((r) {
      Navigator.push(context, CupertinoPageRoute(builder: (context) => AddFestivalPage(restaurant: r, prevPageTitle: "Festivals"))).then((ret) {
        //
        // TODO: Can this be changed to simply call `this._loadData()`?
        //
        DatabaseHelper().loadFestivalsForTownshipId(this.rid).then((lst) {
          setState(() {
            this.items = lst;
          });
        });
      });
    });
  }

  Widget _dismissible(Festival f, Widget child) {
    return Dismissible(
      key: Key("${f.id}"),
      direction: DismissDirection.endToStart,
      child: child,
      background: dismissRemoveContainer(this.context),
      onDismissed: (direction) {
        DatabaseHelper().removeFestival(f);
        this.items.removeWhere((rf) => rf.id == f.id);
      });
  }

  Widget _buildBody() {
    if(this.items.length > 0) {
      var _staticItems = <AbstractPropertyWidget>[
        LinePropertyWidget.from(i18n(this.context).festControlInstructions),
      ];

      var _lv = PropertyListView(
        items: _staticItems + this.items.map((f) {
          return NumericPropertyWidget.from(
            df.format(f.dateTime), f.yellow,
            suffix: i18n(this.context).globTrophySuffix,
            onTapped: (_) => _selectFestival(f),
            onLongPressed: (_) => _editFestivalDataPage(context, f),
            mutateChild: (w) {
              return _dismissible(f, w);
            },
          );
        }).toList(),
      );

      return _lv;
    } else {
      var _emptyLv = PropertyListView(
        items: [
          LinePropertyWidget.from(i18n(this.context).festNoFestivalsAddedYet),
        ]);

      return _emptyLv;
    }
  }

  @override
  Widget build(BuildContext context) {
    var _addButton = CupertinoButton(
        padding: EdgeInsets.all(0),
        child: Icon(CupertinoIcons.add),
        onPressed: _addNewFestival);

    var _navBar = CupertinoNavigationBar(
      middle: Text(i18n(this.context).mainFestivals),
      previousPageTitle: this.r == null ? "..." : this.r.name,
      trailing: _addButton,
    );

    return CupertinoPageScaffold(
      navigationBar: _navBar,
      child: _buildBody()
    );
  }
}
