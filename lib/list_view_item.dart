import 'package:flutter/cupertino.dart';

class ListViewPadding extends StatelessWidget {
  final Widget child;

  ListViewPadding({@required this.child});

  Widget build(BuildContext ctx) {
    return Padding(padding: EdgeInsets.all(8), child: child);
  }
}

class ListViewItem extends StatefulWidget {
  final String title;
  final VoidCallback onTap;
  final VoidCallback onLongTap;
  final Icon icon;

  ListViewItem({this.title = "", this.onTap, this.onLongTap, this.icon});

  @override
  State<StatefulWidget> createState() =>
    _ListViewItemState();
}

class _ListViewItemState extends State<ListViewItem> {
  bool _pressedDown;

  @override
  void initState() {
    super.initState();
    this._pressedDown = false;
  }

  Widget build(BuildContext ctx) {
    var _currentColor;

    if(this.widget.onTap == null) {
      _currentColor = CupertinoColors.black;
    } else {
      if(this._pressedDown) {
        _currentColor = CupertinoColors.lightBackgroundGray;
      } else {
        _currentColor = CupertinoColors.activeBlue;
      }
    }

    var _child = Text(this.widget.title,
        textAlign: TextAlign.center,
        style: TextStyle(color: _currentColor)
    );

    var _icon = this.widget.icon;
    var _paddedIcon = Padding(padding: EdgeInsets.only(right: 8),
        child: _icon);

    var _row = Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children:
          (this.widget.icon == null ?
            <Widget>[] :
            <Widget>[_paddedIcon])
          + <Widget>[_child]);

    var _box = SizedBox(
        height: 28,
        child: _row);

    var _gd = GestureDetector(
      onTapDown: (details) { setState(() { this._pressedDown = true; }); },
      onTapUp:   (details) { setState(() { this._pressedDown = false; }); },
      onTapCancel: () { setState(() { this._pressedDown = false; }); },
      onTap: () => this.widget.onTap == null ? () {} : this.widget.onTap(),
      onLongPress: () => this.widget.onLongTap == null ? () {} : this.widget.onLongTap(),
      child: _box,
      behavior: HitTestBehavior.opaque,
    );

    return ListViewPadding(child: _gd);
  }
}

