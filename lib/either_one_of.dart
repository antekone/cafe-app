dynamic eitherOneOf(object, lst) {
  var ret = lst
      .where((String i) => object.containsKey(i) == true)
      .map((String i) => object[i])
      .toList();

  return ret.length == 0 ? null : ret[0];
}
