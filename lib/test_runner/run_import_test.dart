import 'dart:io' as io;
import 'package:cafe_app/database_helper.dart';

void main() {
  run();
}

Future<void> run() async {
  var fileName = "/data/data/org.antoniak.mycafecalc/xxx/backup.zip";
  var tempDir = await io.Directory.systemTemp.createTemp("import");

  try {
    DatabaseHelper().importFromCSV(fileName, tempDir);
  } finally {
    tempDir.delete(recursive: true);
  }
}
