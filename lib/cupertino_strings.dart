import 'dart:ui';
import 'package:flutter/cupertino.dart';
import "package:intl/intl.dart";
import 'cupertino_i18n/messages_all.dart';

class LocalCupertinoLocalizationsDelegate extends LocalizationsDelegate<CupertinoLocalizations> {
  @override
  bool isSupported(Locale locale) => locale.languageCode == "pl";

  @override
  Future<CupertinoLocalizations> load(Locale locale) => LocalCupertinoLocalizations.load(locale);

  @override
  bool shouldReload(LocalizationsDelegate<CupertinoLocalizations> old) => false;

  @override
  String toString() => 'LocalCupertinoLocalizations.delegate(all locales)';
}

class LocalCupertinoLocalizations implements CupertinoLocalizations {
  LocalCupertinoLocalizations(String locale) {
  }

  static Future<CupertinoLocalizations> load(Locale locale) {
    final String name = locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return LocalCupertinoLocalizations(localeName);
    });
  }

  @override
  String get alertDialogLabel => Intl.message("Alert", name: "alertDialogLabel");

  @override
  String get anteMeridiemAbbreviation => 'AM';

  @override
  String get copyButtonLabel => 'Kopiuj';

  @override
  String get cutButtonLabel => 'Wytnij';

  @override
  DatePickerDateOrder get datePickerDateOrder => DatePickerDateOrder.dmy;

  @override
  DatePickerDateTimeOrder get datePickerDateTimeOrder => DatePickerDateTimeOrder.time_dayPeriod_date;

  @override
  String datePickerDayOfMonth(int dayIndex) => dayIndex.toString();

  @override
  String datePickerHour(int hour) => hour.toString();

  @override
  String datePickerHourSemanticsLabel(int hour) => "godz. " + hour.toString();

  static const List<String> _shortWeekdays = [
    "Pon", "Wt", "Śr", "Czw", "Pt", "Sob", "Niedz",
  ];

  static const List<String> _shortMonths = [
    "Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz",
    "Paź", "Lis", "Gru"
  ];

  static const List<String> _months = [
    "stycznia", "lutego", "marca", "kwietnia", "maja", "czerwca", "lipca",
    "sierpnia", "września", "października", "listopada", "grudnia"
  ];

  @override
  String datePickerMediumDate(DateTime date) {
    return '${_shortWeekdays[date.weekday - DateTime.monday]} '
        '${_shortMonths[date.month - DateTime.january]} '
        '${date.day.toString().padRight(2)}';
  }

  @override
  String datePickerMinute(int minute) => minute.toString().padLeft(2, '0');

  @override
  String datePickerMinuteSemanticsLabel(int minute) {
    var lastMinuteDigit = minute % 10;
    if(minute == 1)
      return '1 minuta';
    else if(lastMinuteDigit > 1 && lastMinuteDigit < 5)
      return "$minute minuty";
    else
      return "$minute minut";
  }

  @override
  String datePickerMonth(int monthIndex) => _months[monthIndex - 1];

  @override
  String datePickerYear(int yearIndex) => yearIndex.toString();

  @override
  String get pasteButtonLabel => 'Wklej';

  @override
  String get postMeridiemAbbreviation => 'PM';

  @override
  String get selectAllButtonLabel => "Zaznacz wszystko";

  @override
  String timerPickerHour(int hour) => hour.toString();

  @override
  String timerPickerHourLabel(int hour) {
    var lastDigit = hour % 10;
    if(hour == 1)
      return 'godzina';
    else if(lastDigit > 1 && lastDigit < 5)
      return "godziny";
    else
      return "godzin";
  }

  @override
  String timerPickerMinute(int minute) => minute.toString();

  @override
  String timerPickerMinuteLabel(int minute) {
    var lastDigit = minute % 10;
    if(minute == 1)
      return 'minuta';
    else if(lastDigit > 1 && lastDigit < 5)
      return "minuty";
    else
      return "minut";
  }

  @override
  String timerPickerSecond(int second) => second.toString();

  @override
  String timerPickerSecondLabel(int second) {
    var lastDigit = second % 10;
    if(second == 1)
      return 'sekunda';
    else if(lastDigit > 1 && lastDigit < 5)
      return "sekundy";
    else
      return "sekund";
  }
}
