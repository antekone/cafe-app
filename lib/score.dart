import 'package:cafe_app/either_one_of.dart';

class Score {
  int _id;
  int _uid;
  int _fid;
  int _rid;
  int _yellow;
  int _tasks;

  Score(this._id, this._fid, this._rid, this._uid, this._yellow, this._tasks);

  Score.map(dynamic object) {
    this._id = object['id'];
    this._uid = eitherOneOf(object, ['uid', 'userId']);
    this._fid = eitherOneOf(object, ['fid', 'festivalId']);
    this._rid = eitherOneOf(object, ['rid', 'townshipId']);
    this._yellow = eitherOneOf(object, ['yellow', 'trophies']);
    this._tasks = object['tasks'];
  }

  int get id => _id;
  int get uid => _uid;
  int get fid => _fid;
  int get rid => _rid;
  int get yellow => _yellow;
  int get tasks => _tasks;

  Map<String, dynamic> toMap() {
    var m = Map<String, dynamic>();
    m['id'] = id;
    m['uid'] = uid;
    m['fid'] = fid;
    m['rid'] = rid;
    m['yellow'] = yellow;
    m['tasks'] = tasks;
    return m;
  }

  Map<String, dynamic> toMapNoId() {
    var m = this.toMap();
    m.remove('id');
    return m;
  }
}
