import 'package:cafe_app/migration/row_mapper_aware.dart';

class Township implements RowMapperAware {
  String _name;
  int _id;

  Township(this._id, this._name);

  Township.map(dynamic obj) {
    this.applyFromMap(obj);
  }

  @override // RowMapperAware
  Township emptyInstance() {
    return Township(null, null);
  }

  @override // MapAware
  void applyFromMap(dynamic obj) {
    this._id = obj['id'];
    this._name = obj['name'];
  }

  int get id => _id;
  String get name => _name;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['id'] = _id;
    map['name'] = _name;
    return map;
  }

  Map<String, dynamic> toMapNoId() {
    var map = toMap();
    map.remove("id");
    return map;
  }
}
