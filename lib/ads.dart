import 'package:flutter/cupertino.dart';
import 'package:cafe_app/debug_utils.dart' as DebugUtils;

String appId;
String adUnitId;

// Sony G8141 device id: DA75BE8D99D535CF95E354973F765029

void initNative() {
  DebugUtils.invoke<void>(debugCode: () {
    // Debug
    appId = "ca-app-pub-3940256099942544~3347511713";
    adUnitId = 'ca-app-pub-3940256099942544/6300978111';
  }, releaseCode: () {
    // Release
    appId = "ca-app-pub-1998569664414077~3078844627";
    adUnitId = "ca-app-pub-1998569664414077/1743735742";
  });
}

Widget nativeAd() {
  return Text("");
}

Widget appContainer(bool showAd, BuildContext ctx, Widget widget) {
    var adColumn = !showAd ? widget : Directionality(textDirection: TextDirection.ltr,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Expanded(child: widget),
              Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [Expanded(child:
                  Container(
                      color: CupertinoColors.black,
                      child: SizedBox(
                          height: 60,
                          child: FittedBox(
                              alignment: Alignment.bottomCenter,
                              fit: BoxFit.contain,
                              child: Container(
                                  color: CupertinoColors.black,
                                  child: nativeAd())))))])]));

    return Container(
        decoration: BoxDecoration(
          boxShadow: [BoxShadow(
            color: CupertinoColors.white,
            offset: Offset(0, 0),
          )],
        ),
        child: adColumn);
}
