import 'package:cafe_app/migration/export_google_drive_page.dart';
import 'package:cafe_app/migration/import_google_drive_page.dart';
import 'package:cafe_app/migration/migration_methods.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/utils/page_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:googleapis/drive/v3.dart';

typedef FunctionCallback = void Function(VoidCallback callback);
typedef GoogleSignInCallback = void Function(GoogleSignInAccount acc,
  Map<String, String> headers);

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>[
    'email',
    DriveApi.DriveFileScope,
  ],
);

class GoogleDriveControlPanel {
  //GoogleSignInAccount _currentUser;
  FunctionCallback setState;
  GoogleSignInCallback loggedInFn;
  BuildContext context;
  GDriveMigrationMethod myMigrationMethod;
  bool export;

  bool _loginInitFailure = false;
  bool _alreadySignedIn = false;
  bool _preauthStep1Completed = false;
  bool _preauthStep2Completed = false;

  GoogleDriveControlPanel(this.setState, this.export);

  void initState(BuildContext context) {
    this.context = context;
    _googleSignIn.isSignedIn().then((bool flag) {
      setState(() {
        this._preauthStep1Completed = true;

        if(flag) {
          _googleSignIn.signInSilently().then((GoogleSignInAccount acc) {
            if(acc != null) {
              this._loggedIn(acc);
            } else {
              this._loginFailed();
            }
          });
        } else {
          this._preauthStep2Completed = true;
        }
      });
    });
  }

  void _loginFailed() {
    setState(() => this._loginInitFailure = true);
  }

  void _loggedIn(GoogleSignInAccount acc) {
    acc.authHeaders.then((headers) {
      setState(() {
        //this._currentUser = acc;
        if(acc != null) {
          this._preauthStep1Completed = true;
          this._preauthStep2Completed = true;
          this._alreadySignedIn = true;
        }
      });

      Widget dest;
      if(this.export) {
        dest = ExportGoogleDrivePage(acc: acc, authHeaders: headers);
      } else {
        dest = ImportGoogleDrivePage(acc: acc, authHeaders: headers);
      }

      PageUtils.push(this.context, dest).then((r) {
        print("forwarding result: $r");
        Navigator.of(this.context).pop(r);
      });
    });
  }

  Widget buildExportPanel(BuildContext context) {
    this.context = context;
    return Column(children: this._buildRows());
  }

  Widget buildImportPanel(BuildContext context) {
    this.context = context;
    return Column(children: this._buildRows());
  }

  Widget _buildLoadingIndicator() {
    var _centeredRow = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [CupertinoActivityIndicator()],
    );

    var _paddedCaption = Padding(
      padding: EdgeInsets.all(16),
      child: Text(i18n(this.context).globPleaseWait));

    var _centeredCol = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [_centeredRow, _paddedCaption],
    );

    var _padded = Padding(
      padding: EdgeInsets.all(8),
      child: _centeredCol,
    );

    return _padded;
  }

  Widget _buildLoginInitFailure() {
    var _centeredRow = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [Icon(FontAwesomeIcons.carCrash)],
    );

    var _paddedCaption = Padding(
        padding: EdgeInsets.all(16),
        child: Text("Google Drive error!"));

    var _centeredCol = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [_centeredRow, _paddedCaption],
    );

    var _padded = Padding(
      padding: EdgeInsets.all(8),
      child: _centeredCol,
    );

    return _padded;
  }

  List<Widget> _buildRows() {
    if(!this._preauthStep1Completed || !this._preauthStep2Completed) {
      // Still loading.
      if(!this._loginInitFailure) {
        return [this._buildLoadingIndicator()];
      } else {
        return [this._buildLoginInitFailure()];
      }
    } else {
      return [
        this._alreadySignedIn == false ? this._buildSignInButton() : _buildSignedInArea(),
      ].where((w) => w != null).toList();
    }
  }

  Widget _buildSignInButton() {
    var button = CupertinoButton(
      child: Text("Sign in to your Google Drive"),
      color: CupertinoColors.activeBlue,
      onPressed: () {
        _googleSignIn.signIn().then((GoogleSignInAccount acc) {
          this._loggedIn(acc);
        });
      },
    );

    var paddedButton = Padding(
      padding: EdgeInsets.only(top: 18.0, bottom: 18.0),
      child: button
    );

    var row = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [paddedButton]
    );

    return row;
  }

  Widget _buildSignedInArea() {
    var row = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 8),
          child: Text("")
        )]
    );

    return row;
  }
}
