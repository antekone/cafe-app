import 'package:cafe_app/migration/google_drive_control_panel.dart';
import 'package:cafe_app/property_list_view.dart';
import 'package:flutter/cupertino.dart';

typedef FunctionCallback = void Function(VoidCallback callback);
typedef NextEnabledUpdateCallback = void Function(bool callback);

enum MigrationMode {
  Import, Export
}

abstract class MigrationMethod implements ComboInner {
  FunctionCallback setState;
  NextEnabledUpdateCallback nextEnabled;
  MigrationMode mode;

  MigrationMethod();

  String name();
  Widget controlExportPanelWidget(BuildContext context);
  Widget controlImportPanelWidget(BuildContext context);
  void setStateUpdateFn(FunctionCallback fn) => this.setState = fn;
  void setNextEnabledUpdateFn(NextEnabledUpdateCallback fn) => this.nextEnabled = fn;
  void lateInit() { }
  void onShowInit(BuildContext context) { }
  void setMode(MigrationMode mm) => this.mode = mm;

  String getValueString() {
    return this.name();
  }
}

class GDriveMigrationMethod extends MigrationMethod {
  GoogleDriveControlPanel cp;

  GDriveMigrationMethod();

  @override
  void lateInit() {
    this.cp = GoogleDriveControlPanel(this.setState, this.mode == MigrationMode.Export);
    this.cp.myMigrationMethod = this;
  }

  @override
  void onShowInit(BuildContext context) {
    this.cp.initState(context);
  }

  @override String name() {
    return "Google Drive";
  }

  Widget controlExportPanelWidget(BuildContext context) {
    return cp.buildExportPanel(context);
  }

  Widget controlImportPanelWidget(BuildContext context) {
    return cp.buildImportPanel(context);
  }
}

class EMailMigrationMethod extends MigrationMethod {
  EMailMigrationMethod();

  @override String name() {
    return "Send by E-Mail";
  }

  Widget controlExportPanelWidget(BuildContext context) {
    return Text("Not ready yet!");
  }

  Widget controlImportPanelWidget(BuildContext context) {
    return Text("email import");
  }
}

class PhoneMemoryMigrationMethod extends MigrationMethod {
  PhoneMemoryMigrationMethod();

  @override String name() {
    return "Phone memory";
  }

  Widget controlExportPanelWidget(BuildContext context) {
    return Text("Not ready yet!");
  }

  Widget controlImportPanelWidget(BuildContext context) {
    return Text("phone memory import");
  }
}

class SDCardMigrationMethod extends MigrationMethod {
  SDCardMigrationMethod();

  @override String name() {
    return "SD Card memory";
  }

  Widget controlExportPanelWidget(BuildContext context) {
    return Text("Not ready yet!");
  }

  Widget controlImportPanelWidget(BuildContext context) {
    return Text("sdcard import");
  }
}

var kMigrationMethods = [
  GDriveMigrationMethod(),
//  EMailMigrationMethod(),
//  PhoneMemoryMigrationMethod(),
//  SDCardMigrationMethod(),
];

void initMigrationMethods(FunctionCallback setState, MigrationMode mm) {
  for(var method in kMigrationMethods) {
    method.setStateUpdateFn(setState);
    method.setMode(mm);
    method.lateInit();
  }
}
