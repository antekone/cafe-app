import 'package:flutter/cupertino.dart';
import 'package:cafe_app/migration/migration_methods.dart';
import 'package:cafe_app/property_list_view.dart';
import 'package:cafe_app/ads.dart' as Ads;

class ExportBackupPage extends StatefulWidget {
  @override
  _ExportBackupPageState createState() => _ExportBackupPageState();
}

class _ExportBackupPageState extends State<ExportBackupPage> {
  bool _nextEnabled = true;
  int _currentMigrationMethod = -1;

  @override
  void initState() {
    super.initState();
    initMigrationMethods(this.setState, MigrationMode.Export);

    setState(() {
      this._nextEnabled = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text("Creating a backup"),
        trailing: CupertinoButton(
          padding: EdgeInsets.all(0),
          child: Text(""),
          onPressed: this._nextEnabled ? () => _nextStep : null)),
      child: SafeArea(
        child: PropertyListView(
          drawLines: false,
          items: <AbstractPropertyWidget>[] +
            this._buildSignInSections() +
            this._buildMigrationSpecificSection(),
          ),
        ),
    );
  }

  List<AbstractPropertyWidget> _buildSignInSections() {
    return [
      LinePropertyWidget.from("Select your destination device:", bold: false, center: false),
      ActionSheetPropertyWidget<MigrationMethod>.from(
        this.context,
        "Method:",
        kMigrationMethods,
        message: "Select your destination device:",
        stateUpdate: (int idx) =>
          setState(() {
            if(idx != this._currentMigrationMethod) {
              this._currentMigrationMethod = idx;
              this._nextEnabled = false;
              this._initMigrationMethod(idx);
            }
          }),
        currentObject: this._currentMigrationMethod),
    ];
  }

  void _initMigrationMethod(int idx) {
    if(idx >= 0 && idx < kMigrationMethods.length) {
      kMigrationMethods[idx].onShowInit(this.context);
    }
  }

  List<AbstractPropertyWidget> _buildMigrationSpecificSection() {
    var idx = this._currentMigrationMethod;
    if(idx >= 0 && idx < kMigrationMethods.length) {
      var controlPanelWidget =
        kMigrationMethods[idx].controlExportPanelWidget(this.context);

      kMigrationMethods[idx].setNextEnabledUpdateFn((f) {
        setState(() {
          this._nextEnabled = f;
        });
      });

      return [
        IdentityPropertyWidget(controlPanelWidget, center: true),
      ].where((w) => w != null).toList();
    } else {
      return [];
    }
  }

  void _nextStep() {
    print("next");
  }
}
