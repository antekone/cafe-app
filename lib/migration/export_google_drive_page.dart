import 'dart:io' as io;

import 'package:cafe_app/database_helper.dart';
import 'package:cafe_app/image_box.dart';
import 'package:cafe_app/property_list_view.dart';
import 'package:cafe_app/utils/google_http_client.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:googleapis/drive/v3.dart';
import 'package:intl/intl.dart';

class ExportGoogleDrivePage extends StatefulWidget {
  final GoogleSignInAccount acc;
  final Map<String, String> authHeaders;

  ExportGoogleDrivePage({this.acc, this.authHeaders});

  @override
  _ExportGoogleDrivePageState createState() => _ExportGoogleDrivePageState();
}

class CreateBackupResult {
  bool successFlag;
  String filename;
  io.Directory tempDir;
  String errorMsg;
  int fileSize;

  static CreateBackupResult success(io.Directory tempDir, String fn) {
    var r = CreateBackupResult();
    r.filename = fn;
    r.fileSize = io.File(fn).statSync().size;
    r.successFlag = true;
    r.tempDir = tempDir;
    return r;
  }

  static CreateBackupResult failure(String msg) {
    var r = CreateBackupResult();
    r.errorMsg = msg;
    r.successFlag = false;
    return r;
  }
}

class _ExportGoogleDrivePageState extends State<ExportGoogleDrivePage> {
  GoogleHttpClient _http;
  bool autoPopCancelled = false;
  bool createPhaseFinished = false;
  bool lastPhaseFinished = false;
  bool uploadAllowed = false;
  bool uploadingPhaseActive = false;
  CreateBackupResult createPhaseResult;

  String currentTargetDirectoryName = "MyCafeCalculator";
  String currentTargetFilename = "";
  String currentTargetFilesize = "";

  bool uploadResultSuccess = false;
  bool uploadFinished = false;
  bool uploadingPhaseResult = false;

  // Always called from setState()!
  void updatePhases() {
    if(this.createPhaseFinished) {
      this.lastPhaseFinished = true;
    }

    if(this.lastPhaseFinished && this.createPhaseResult.successFlag) {
      this.uploadAllowed = true;
    }
    this.uploadAllowed = true;
  }

  String generateFilesizeString(String path) {
    var file = io.File(path);
    var size = file.statSync().size;

    var nf = NumberFormat();
    return "${nf.format(size)} bytes";
  }

  String baseName(String fullPath) {
    var idx = fullPath.lastIndexOf("/");
    if(idx == -1) {
      return fullPath;
    } else {
      return fullPath.substring(idx + 1);
    }
  }

  @override
  void initState() {
    super.initState();
    this._http = GoogleHttpClient(this.widget.authHeaders);

    Future.delayed(Duration(seconds: 1)).then((r) {
      this.createBackup().then((CreateBackupResult result) {
        setState(() {
          this.currentTargetFilename = baseName(result.filename);
          this.currentTargetFilesize = generateFilesizeString(result.filename);
          this.createPhaseFinished = true;
          this.createPhaseResult = result;
          this.updatePhases();
        });
      });
    });
  }

  Future<CreateBackupResult> createBackup() async {
    var result = await DatabaseHelper().exportToCSV();
    if(result.isValue) {
      var tuple = result.asValue.value;
      return CreateBackupResult.success(tuple.item1, tuple.item2);
    } else {
      return CreateBackupResult.failure(result.asError.toString());
    }
  }

  Future<List<File>> getDriveContents() async {
    return (await DriveApi(http()).files.list(corpora: 'user')).files;
  }

  String getDriveTargetDirectoryId(List<File> files) {
    var items = files.where((f) => f.name == this.currentTargetDirectoryName).toList();
    if(items.length == 0)
      return null;
    else if(items.length > 0) {
      return items[0].id;
    } else {
      return null;
    }
  }

  Future<String> createDriveTargetDirectory(String dirName) async {
    File f = File.fromJson({
      "name": dirName,
      "mimeType": "application/vnd.google-apps.folder",
    });


    File nf = await DriveApi(http()).files.create(f);
    if(nf == null || nf.id == null) {
      return null;
    }

    print("created root directory: ${nf.id}");
    return nf.id;
  }

  Future<bool> startUpload() async {
    try {
      print("Reading contents...");
      List<File> files = await getDriveContents();

      print("Checking for existence for target directory...");
      String dirId = this.getDriveTargetDirectoryId(files);
      if(dirId == null) {
        dirId = await this.createDriveTargetDirectory(this.currentTargetDirectoryName);
        if(dirId == null) {
          this.uploadResultSuccess = false;
          this.uploadFinished = true;
          return false;
        }
      }

      print("using parent directory: $dirId");
      File reqFile = File.fromJson({
        "name": this.currentTargetFilename,
        "parents": [ dirId ],
        "mimeType": "application/octet-stream"
      });

      var len = this.createPhaseResult.fileSize;
      var stream = io.File(this.createPhaseResult.filename).openRead();
      var media = Media(stream, len);

      File uploadedFile = await DriveApi(http()).files.create(reqFile, uploadMedia: media);
      if(uploadedFile == null || uploadedFile.id == null) {
        return false;
      }

      print("Upload complete.");
      return true;
    } catch(e) {
      print("Error during upload step of backup: $e");
      return false;
    } finally {
      print("Removing temp dir: ${this.createPhaseResult.tempDir.path}");
      this.createPhaseResult.tempDir.delete(recursive: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    var navBar = CupertinoNavigationBar(
      middle: Text("Export to Google Drive"),
      trailing: this.uploadAllowed ?
        CupertinoButton(
          padding: EdgeInsets.all(0),
          child: Text("Upload"),
          onPressed: () {
            setState(() { uploadingPhaseActive = true; });
            startUpload().then((bool f) {
              setState(() {
                print("Upload job finished, result: $f");
                this.uploadingPhaseActive = false;
                this.uploadFinished = true;
                this.uploadingPhaseResult = f;
                this.uploadAllowed = false;
              });

              this.autoPop(f);
            });
          }) : null,
    );

    return CupertinoPageScaffold(
      navigationBar: navBar,
      child: SafeArea(
        child: this.buildPage()
      )
    );
  }

  Widget buildWorkingIndicator(String msg) {
    var _centeredRow = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [CupertinoActivityIndicator()],
    );

    var _paddedCaption = Padding(
      padding: EdgeInsets.all(16),
      child: Text(msg));

    var _centeredCol = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [_centeredRow, _paddedCaption],
    );

    var _padded = Padding(
      padding: EdgeInsets.all(8),
      child: _centeredCol,
    );

    return _padded;
  }

  Widget buildSummaryPage() {
    return PropertyListView(
      drawLines: true,
      items: [
        TextPropertyWidget.from("Directory", this.currentTargetDirectoryName),
        TextPropertyWidget.from("File name", this.currentTargetFilename),
        TextPropertyWidget.from("Size", this.currentTargetFilesize),
      ]
    );
  }

  Widget buildUploadFinishedScreen() {
    Widget imageBox;

    if(this.uploadingPhaseResult) {
      imageBox = ImageBox(imagePath: "assets/succeeded.png", width: 64, height: 64);
    } else {
      imageBox = ImageBox(imagePath: "assets/failed.png", width: 64, height: 64);
    }

    var _centeredRow = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [imageBox],
    );

    var _paddedCaption = Padding(
      padding: EdgeInsets.all(16),
      child: this.uploadingPhaseResult ?
        Text("Backup created OK!") :
        Text("Error while creating backup!"));

    var _centeredCol = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [_centeredRow, _paddedCaption],
    );

    var _padded = Padding(
      padding: EdgeInsets.all(8),
      child: _centeredCol,
    );

    var _gestureDetector = GestureDetector(
      child: _padded,
      onTap: () {
        this.autoPopCancelled = true;
        Navigator.of(this.context).pop(this.uploadingPhaseResult);
      }
    );

    return _gestureDetector;
  }

  void autoPop(bool backToTownshipListFlag) {
    Future.delayed(Duration(seconds: 3), () {
      if(!this.autoPopCancelled)
        Navigator.of(this.context).pop(backToTownshipListFlag);
    });
  }

  Widget buildPage() {
    if(this.uploadFinished) {
      return buildUploadFinishedScreen();
    }

    if(this.lastPhaseFinished && !this.uploadingPhaseActive) {
      return buildSummaryPage();
    } else {
      if(!this.createPhaseFinished || this.uploadingPhaseActive) {
        return buildWorkingIndicator("Creating backup, please wait...");
      } else {
        return buildWorkingIndicator("Please wait...");
      }
    }
  }

  GoogleHttpClient http() => this._http;
}
