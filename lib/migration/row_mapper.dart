import 'package:cafe_app/migration/map_aware.dart';

typedef TypedCallback = void Function(Map<String, Object> objMap);

class RowMapper {
  int rowIdx = 1;
  List<List> rows;
  var captions = List<String>();

  RowMapper(this.rows) {
    this.captions = this.rows[0].map((r) => r.toString()).toList();
  }

  Map<String, Object> translateRow(List<Object> items) {
    return items.asMap().map((index, data) {
      return MapEntry(this.captions[index], data);
    });
  }

  void eachObject(TypedCallback callback) async {
    rows.skip(1).forEach((r) async => callback(this.translateRow(r)));
  }
}
