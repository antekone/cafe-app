import 'package:cafe_app/loading_page.dart';
import 'package:cafe_app/property_list_view.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/township.dart';
import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:optional/optional.dart';

class AddNewDuePage extends StatefulWidget {
  final Township township;

  AddNewDuePage({@required this.township});

  @override
  _AddNewDuePageState createState() => _AddNewDuePageState();
}

// Return Some(collectionId) if user should be redirected to collection edition
// page. None if the user should be redirected to home page.

typedef StateSetterFunction = Function(VoidCallback func);

abstract class AbstractDueType implements ComboInner {
  StateSetterFunction setState;
  BuildContext context;

  List<AbstractPropertyWidget> getPropertyFields();
  bool validateParameters();
}

class CommonGoalDueType extends AbstractDueType {
  @override
  String getValueString() {
    return "Każdy płaci ile chce";
  }

  List<AbstractPropertyWidget> getPropertyFields() {
    return <AbstractPropertyWidget>[
    ];
  }

  bool validateParameters() {
    return true;
  }
}

class IndividualGoalDueType extends AbstractDueType {
  int coinCount = 0;
  int diamondCount = 0;

  @override
  String getValueString() {
    return "Każdy płaci stałą stawkę";
  }

  List<AbstractPropertyWidget> getPropertyFields() {
    return <AbstractPropertyWidget>[
      NumericPropertyWidget.from("Ile monet?", this.coinCount,
        editTitle: "Jaka należność?",
        editBody: "Ile monet każdy gracz powinien zapłacić?",
        setCallback: (n) => super.setState(() {
          this.coinCount = n;
        })),

      NumericPropertyWidget.from("Ile diamentów?", this.diamondCount,
        editTitle: "Jaka należność?",
        editBody: "Ile diamentów każdy gracz powinien zapłacić?",
        suffix: (n) => i18n(super.context).globDiamondSuffix(n),
        setCallback: (n) => super.setState(() {
          this.diamondCount = n;
        }),
      ),
    ];
  }

  bool validateParameters() {
    return this.coinCount > 0 && this.diamondCount > 0;
  }
}

class _AddNewDuePageState extends State<AddNewDuePage> {
  bool loading;
  DateTime selectedDate;
  int weekNumber;
  List<AbstractDueType> dueTypes;
  int selectedDueType = 1;

  @override
  void initState() {
    super.initState();

    setState(() {
      this.dueTypes = <AbstractDueType>[
        CommonGoalDueType(),
        IndividualGoalDueType()
      ];

      this.dueTypes.forEach((e) {
        e.setState = this.setState;
        e.context = this.context;
      });

      this.loading = false;
      this.selectedDate = DateTime.now();
      this.weekNumber = this.calcWeekNumber(this.selectedDate);
    });
  }

  int calcWeekNumber(DateTime date) {
    int dayOfYear = int.parse(DateFormat("D").format(date));
    return ((dayOfYear - date.weekday + 10) / 7).floor();
  }

  Widget buildBody() {
    var duePropertyFields = this.dueTypes[this.selectedDueType]?.getPropertyFields();
    var sections = <AbstractPropertyWidget>[
      DatePropertyWidget.from(i18n(this.context).festDate, this.selectedDate,
        setCallback: (DateTime newDate) => setState(() {
          this.selectedDate = newDate;
          this.weekNumber = this.calcWeekNumber(newDate);
          print("wn=${this.weekNumber}");
        })
      ),

      NumericPropertyWidget.from("Week number", this.weekNumber,
        suffix: (n) => "Week ${n}",
        inactiveText: true,
        includeOriginalValueInSuffix: false),

      ActionSheetPropertyWidget.from(this.context, "Rodzaj składki", this.dueTypes,
        currentObject: this.selectedDueType,
        message: "Jak rozliczać płacących?",
        stateUpdate: (int idx) {
          setState(() {
            if(idx != -1) {
              this.selectedDueType = idx;
            }
          });
        }),

        (duePropertyFields.length > 0) ? LinePropertyWidget.from(i18n(this.context).festChoose) : null,
    ].where((i) => i != null).toList() + duePropertyFields;

    return PropertyListView(items: sections);
  }

  Widget buildIndicatorOrPage() {
    if(this.loading) {
      return LoadingPage(withScaffold: false);
    } else {
      return this.buildBody();
    }
  }

  bool validateForm() {
    return this.dueTypes[this.selectedDueType].validateParameters();
  }

  Future<int> addToDatabase() async {
    return 0;
  }

  @override
  Widget build(BuildContext context) {
    var _navigationBar = CupertinoNavigationBar(
      middle: Text("Nowa składka"), // TODO: I18N
      trailing: CupertinoButton(
        padding: EdgeInsets.all(0),
        child: Text(i18n(this.context).globCreate),
        onPressed: !this.validateForm() ? null : () {
          this.addToDatabase().then((id) =>
            Navigator.pop(this.context, Optional.ofNullable(id))
          );
        }),
    );

    return CupertinoPageScaffold(
      navigationBar: _navigationBar,
      child: SafeArea(child: this.buildIndicatorOrPage()),
    );
  }
}
