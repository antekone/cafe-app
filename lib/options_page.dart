import 'package:cafe_app/about_page.dart';
import 'package:cafe_app/debug_page.dart';
import 'package:cafe_app/global_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:cafe_app/migration/import_backup_page.dart';
import 'package:cafe_app/migration/export_backup_page.dart';
import 'package:cafe_app/property_list_view.dart';
import 'package:cafe_app/utils/page_utils.dart';
import 'package:cafe_app/debug_utils.dart' as DebugUtils;
import 'package:package_info/package_info.dart';

class DebugChooseExampleType implements ComboInner {
  String itemText;
  DebugChooseExampleType(this.itemText);
  String getValueString() => itemText;
}

typedef DarkModeChangedCallback = void Function(bool flag);

class OptionsPage extends StatefulWidget {
  OptionsPage(this.darkModeChanged);

  DarkModeChangedCallback darkModeChanged;

  @override
  _OptionsPageState createState() => _OptionsPageState();
}

class _OptionsPageState extends State<OptionsPage> {
  int chooseExampleCurrentSelection = -1;
  PackageInfo package;
  bool darkModeSetting = false;

  @override
  void initState() {
    super.initState();
    PackageInfo.fromPlatform().then((pi) => setState(() {
      this.package = pi;
    }));
  }

  Widget _buildOptionsList() {
    var debugItems = <AbstractPropertyWidget>[];
    DebugUtils.run(() {
      var debugChooseExample = [
        DebugChooseExampleType("Item 1"),
        DebugChooseExampleType("Item 2"),
        DebugChooseExampleType("Item 3"),
      ];

      debugItems = <AbstractPropertyWidget>[
        LinePropertyWidget.from("Debug options"),
        ActionSheetPropertyWidget<DebugChooseExampleType>.from(
          this.context,
          "Select:",
          debugChooseExample,
          message: "Select something:",
          stateUpdate: (int idx) {},
          currentObject: this.chooseExampleCurrentSelection),
        TextPropertyWidget.from("Debug page...", "",
          onTapped: (String _x) {
            PageUtils.push(this.context, DebugPage());
          }),
      ];
    });

    var version = this.package == null ? "n/a" : this.package.version;

    return PropertyListView(
      drawLines: true,
      items: [
        TextPropertyWidget.from("App version", "v$version",
          boldName: false, underlinedName: false,
          onTapped: (String currentValue) {
            PageUtils.push(this.context, AboutPage(previousPageTitle: "Options"));
          }),

        // Dark mode is disabled for now, because several Cupertino-styled widgets
        // hardcode colors. Until this is resolved by Flutter/Widget authors,
        // Dark Mode can't happen.

        // If this won't be resolved in several months, I'll try to fork those
        // widgets and perform necessary changes myself.

//        LinePropertyWidget.from("Visual styles"),
//        this._buildVisualStyles(),

        // ----
        LinePropertyWidget.from("Import/Export"),
        this._buildExportEntry(),
        this._buildImportEntry(),
        // LinePropertyWidget.from("Enable Ads"),
        // this._buildAdsEntry(),
      ] + debugItems,
    );
  }

  // ignore: unused_element
  AbstractPropertyWidget _buildVisualStyles() {
    return BooleanPropertyWidget(
      name: "Dark mode",
      defaultValue: GlobalConfig.get().enableDarkMode,
      getCallback: () => GlobalConfig.get().enableDarkMode,
      setCallback: (bool f) {
        // Forward this request to the caller frame.
        this.widget.darkModeChanged(f);
        this.darkModeSetting = f;
      });
  }

  AbstractPropertyWidget _buildImportEntry() {
    return TextPropertyWidget.from("Import from backup...", "",
      onTapped: _importBackupTapped);
  }

  AbstractPropertyWidget _buildExportEntry() {
    return TextPropertyWidget.from("Create backup...", "",
      onTapped: _exportBackupTapped);
  }

  //AbstractPropertyWidget _buildAdsEntry() {
    //return BooleanPropertyWidget(name: "Ads enabled", defaultValue: false);
  //}

  void doublePopIfNeeded(bool result) {
    if(result != null && result == true) {
      // Automatically pop to the township list, if ExportBackupPage has
      // requested it.
      Navigator.of(this.context).pop();
    }
  }

  void _exportBackupTapped(String _dummy) =>
      PageUtils.push<bool>(context, ExportBackupPage()).then(doublePopIfNeeded);

  void _importBackupTapped(String _dummy) =>
      PageUtils.push<bool>(context, ImportBackupPage()).then(doublePopIfNeeded);

  @override
  Widget build(BuildContext context) {
    var _navigationBar = CupertinoNavigationBar(
      middle: Text("Options"),
    );

    return CupertinoPageScaffold(
      navigationBar: _navigationBar,
      child: SafeArea(
        child: _buildOptionsList(),
      )
    );
  }
}
