// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a pl locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'pl';

  static m0(value) => "${Intl.plural(value, zero: 'diamentów', one: 'diament', two: 'diamenty', many: 'diamentów', other: 'diamenty')}";

  static m1(value) => "${Intl.plural(value, zero: 'punktów', one: 'punkt', two: 'punkty', many: 'punktów', other: 'punkty')}";

  static m2(value) => "${Intl.plural(value, zero: 'zadań', one: 'zadanie', two: 'zadania', many: 'zadań', other: 'zadania')}";

  static m3(value) => "${Intl.plural(value, zero: 'rubinów', one: 'rubin', two: 'rubiny', many: 'rubinów', other: 'rubiny')}";

  static m4(value) => "${Intl.plural(value, zero: 'trofeów', one: 'trofeum', two: 'trofea', many: 'trofeów', other: 'trofea')}";

  static m5(minValue, maxValue) => "Ile zadań zostało zakończonych przez tego gracza?\n\nWpisz ilość pomiędzy ${minValue} a ${maxValue}.";

  static m6(minValue, maxValue) => "Ile trofeów zdobył ten gracz?\n\nWpisz ilość pomiędzy ${minValue} a ${maxValue}.";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "aboutAppName" : MessageLookupByLibrary.simpleMessage("Kalkulator MyCafe"),
    "aboutDedicatedTo" : MessageLookupByLibrary.simpleMessage("Dedykowany dla"),
    "aboutOpenBlog" : MessageLookupByLibrary.simpleMessage("Otwórz blog"),
    "aboutOpenSource" : MessageLookupByLibrary.simpleMessage("Otwórz kod źródłowy"),
    "aboutPageTitle" : MessageLookupByLibrary.simpleMessage("O aplikacji"),
    "aboutThirdpartyStuff" : MessageLookupByLibrary.simpleMessage("Dodatkowe biblioteki:"),
    "aboutTranslationAuthor" : MessageLookupByLibrary.simpleMessage("Valergia"),
    "aboutTranslationBy" : MessageLookupByLibrary.simpleMessage("Tłumaczenie przez"),
    "aboutWrittenBy" : MessageLookupByLibrary.simpleMessage("Stworzony przez"),
    "aboutWrittenInFlutter" : MessageLookupByLibrary.simpleMessage("Napisane za pomocą Fluttera!"),
    "editDescSuffixMinimum" : MessageLookupByLibrary.simpleMessage("Minimum"),
    "editRemoveSomePlayersFirst" : MessageLookupByLibrary.simpleMessage("Najpierw usuń któregoś gracza z tego festynu!"),
    "emailFeedbackSubject" : MessageLookupByLibrary.simpleMessage("Opinia na temat Kalkulatora MyCafe"),
    "emailNegativeFeedbackBody" : MessageLookupByLibrary.simpleMessage("Cześć. Nie podoba mi się Twój Kalkulator MyCafe. Oto dlaczego:\n\n"),
    "festAddPlayer" : MessageLookupByLibrary.simpleMessage("Dodaj gracza do festynu"),
    "festBackButtonTitle" : MessageLookupByLibrary.simpleMessage("Festyn"),
    "festCheckYourPoints" : MessageLookupByLibrary.simpleMessage("Upewnij się, że punkty zostały prawidłowo rozdane."),
    "festChoose" : MessageLookupByLibrary.simpleMessage("Wybierz:"),
    "festChooseProperties" : MessageLookupByLibrary.simpleMessage("Uzupełnij dane:"),
    "festControlInstructions" : MessageLookupByLibrary.simpleMessage("Przesuń w lewo, by usunąć. Dotknij, by edytować."),
    "festDate" : MessageLookupByLibrary.simpleMessage("Data"),
    "festDiamondCount" : MessageLookupByLibrary.simpleMessage("Ilość diamentów"),
    "festDiamondReceivedTitle" : MessageLookupByLibrary.simpleMessage("Ile diamentów masz do rozdania?"),
    "festDiamonds" : MessageLookupByLibrary.simpleMessage("Diamenty"),
    "festEditPageTitle" : MessageLookupByLibrary.simpleMessage("Edycja festynu"),
    "festNeedMorePlayers" : MessageLookupByLibrary.simpleMessage("Musisz dodać więcej graczy do tego miasteczka!"),
    "festNoFestivalsAddedYet" : MessageLookupByLibrary.simpleMessage("Nie dodano jeszcze żadnych festynów."),
    "festNoPlayersAddedYet" : MessageLookupByLibrary.simpleMessage("Dodaj graczy do tego festynu!\n\nDotknij napis \'Dodaj gracza\' w prawym górnym rogu ekranu."),
    "festNoPlayersAvailable" : MessageLookupByLibrary.simpleMessage("(brak)"),
    "festOnlyTrophies" : MessageLookupByLibrary.simpleMessage("Tylko trofea"),
    "festPlayerList" : MessageLookupByLibrary.simpleMessage("Lista uczestników poniżej.\n\nDotknij imię, aby kogoś oznaczyć."),
    "festPlayerRemoved" : MessageLookupByLibrary.simpleMessage("(usunięty)"),
    "festQuestCount" : MessageLookupByLibrary.simpleMessage("Ilość zadań"),
    "festQuestReceivedTitle" : MessageLookupByLibrary.simpleMessage("Ile zadań zostało przez wszystkich wykonanych?"),
    "festQuests" : MessageLookupByLibrary.simpleMessage("Zadania"),
    "festRubies" : MessageLookupByLibrary.simpleMessage("Rubiny"),
    "festRubyCount" : MessageLookupByLibrary.simpleMessage("Ilość rubinów"),
    "festRubyReceivedTitle" : MessageLookupByLibrary.simpleMessage("Ile rubinów masz do rozdania?"),
    "festSelectPlayer" : MessageLookupByLibrary.simpleMessage("Dotknij imię gracza, by go wybrać."),
    "festSetQuestPointsTitle" : MessageLookupByLibrary.simpleMessage("Wpisz ilość zadań"),
    "festSetTrophyPointsTitle" : MessageLookupByLibrary.simpleMessage("Wpisz ilość trofeów"),
    "festSummary" : MessageLookupByLibrary.simpleMessage("Pozostało:"),
    "festTrophiesReceivedTitle" : MessageLookupByLibrary.simpleMessage("Ile trofeów zdobyliście na festynie?"),
    "festTrophyCount" : MessageLookupByLibrary.simpleMessage("Ilość trofeów"),
    "festUseQuests" : MessageLookupByLibrary.simpleMessage("Dodaj ilość zadań"),
    "globAdd" : MessageLookupByLibrary.simpleMessage("Dodaj"),
    "globBack" : MessageLookupByLibrary.simpleMessage("Wstecz"),
    "globCancel" : MessageLookupByLibrary.simpleMessage("Anuluj"),
    "globCreate" : MessageLookupByLibrary.simpleMessage("Utwórz"),
    "globDiamondSuffix" : m0,
    "globDoNothing" : MessageLookupByLibrary.simpleMessage("Nie rób nic"),
    "globHello" : MessageLookupByLibrary.simpleMessage("Hejka!"),
    "globLoading" : MessageLookupByLibrary.simpleMessage("Ładowanie..."),
    "globOk" : MessageLookupByLibrary.simpleMessage("OK"),
    "globPleaseWait" : MessageLookupByLibrary.simpleMessage("Proszę czekać..."),
    "globPointSuffix" : m1,
    "globQuestPoints" : MessageLookupByLibrary.simpleMessage("Zadania"),
    "globQuestSuffix" : m2,
    "globQuestion" : MessageLookupByLibrary.simpleMessage("Pytanie"),
    "globRemove" : MessageLookupByLibrary.simpleMessage("Usuń"),
    "globRubySuffix" : m3,
    "globSorry" : MessageLookupByLibrary.simpleMessage("Przykro mi!"),
    "globThankYou" : MessageLookupByLibrary.simpleMessage("Dziękuję!"),
    "globTrophyPoints" : MessageLookupByLibrary.simpleMessage("Trofea"),
    "globTrophySuffix" : m4,
    "globUpdate" : MessageLookupByLibrary.simpleMessage("Aktualizuj"),
    "globVoteDialogQuestion" : MessageLookupByLibrary.simpleMessage("Czy uważasz, że ta apka jest przydatna?"),
    "globVoteNoAnswer" : MessageLookupByLibrary.simpleMessage("Przykro mi, że apka nie spełnia Twoich oczekiwań. Jeśli masz jakieś sugestie, możesz się skontaktować z autorem bezpośrednio! Czy zechcesz teraz stworzyć wiadomość e-mail?"),
    "globVoteYesAnswer" : MessageLookupByLibrary.simpleMessage("Czy zechcesz ocenić tą apkę w Sklepie Play, aby inni też mogli się o tym dowiedzieć?"),
    "globWarning" : MessageLookupByLibrary.simpleMessage("Uwaga!"),
    "mainAbout" : MessageLookupByLibrary.simpleMessage("Informacje o aplikacji"),
    "mainAddNewPlayer" : MessageLookupByLibrary.simpleMessage("Dodaj nowego gracza"),
    "mainFestivalHistory" : MessageLookupByLibrary.simpleMessage("Historia festynów"),
    "mainFestivals" : MessageLookupByLibrary.simpleMessage("Festyny"),
    "mainManagePlayers" : MessageLookupByLibrary.simpleMessage("Zarządzanie listą graczy"),
    "mainManagement" : MessageLookupByLibrary.simpleMessage("Zarządzanie"),
    "mainNewFestival" : MessageLookupByLibrary.simpleMessage("Nowy festyn"),
    "mainPlayers" : MessageLookupByLibrary.simpleMessage("Członkowie"),
    "mainRemoveTownshipBody" : MessageLookupByLibrary.simpleMessage("Czy chcesz usunąć to miasteczko, jego wszystkich graczy i jego wszystkie festyny?"),
    "mainRemoveTownshipTitle" : MessageLookupByLibrary.simpleMessage("Usuń to miasteczko"),
    "mainSelectOption" : MessageLookupByLibrary.simpleMessage("Wybierz właściwą opcję z listy poniżej."),
    "playerAdd" : MessageLookupByLibrary.simpleMessage("Dodaj gracza"),
    "playerAddNew" : MessageLookupByLibrary.simpleMessage("Dodaj nowego gracza..."),
    "playerInstructionText" : MessageLookupByLibrary.simpleMessage("Przesuń w lewo, by usunąć gracza. Dotknij dłużej, by zmienić jego imię na inne."),
    "playerName" : MessageLookupByLibrary.simpleMessage("Imię gracza"),
    "playerNewNameBody" : MessageLookupByLibrary.simpleMessage("Jak nazywa się nowy gracz?"),
    "playerRename" : MessageLookupByLibrary.simpleMessage("Zmień nazwę"),
    "playerRenameBody" : MessageLookupByLibrary.simpleMessage("Jak brzmi nowe imię dla tego gracza?"),
    "playerRenameTitle" : MessageLookupByLibrary.simpleMessage("Zmień imię gracza"),
    "setQuestPointsBody" : m5,
    "setTrophyPointsBody" : m6,
    "townAddBody" : MessageLookupByLibrary.simpleMessage("Wpisz tu nazwę swojego nowego miasteczka.\n\nGdy skończysz pisać, dotknij napis \'Dodaj\' w prawym górnym rogu ekranu."),
    "townAddTitle" : MessageLookupByLibrary.simpleMessage("Dodaj Miasteczko"),
    "townList" : MessageLookupByLibrary.simpleMessage("Moje miasteczka"),
    "townListUpper" : MessageLookupByLibrary.simpleMessage("MOJE MIASTECZKA"),
    "townNothingAddedYet" : MessageLookupByLibrary.simpleMessage("Musisz dodać jakieś miasteczko!\n\nAby to zrobić, dotknij ikonę plusa w prawym górnym rogu ekranu i postępuj zgodnie z dalszymi instrukcjami."),
    "townSwipeLeftToRemove" : MessageLookupByLibrary.simpleMessage("(przesuń w lewo, by usunąć)"),
    "voteEMail" : MessageLookupByLibrary.simpleMessage("E-Mail"),
    "voteNo" : MessageLookupByLibrary.simpleMessage("Nie 😟"),
    "voteRateIt" : MessageLookupByLibrary.simpleMessage("Oceń"),
    "voteYes" : MessageLookupByLibrary.simpleMessage("Tak! 😀")
  };
}
