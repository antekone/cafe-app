import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:cafe_app/text_input_alert_dialog.dart';
import 'package:cafe_app/strings.dart';

const double _kPickerSheetHeight = 216.0;
const double _kPickerItemHeight = 32.0;

typedef CustomValidator<T> = bool Function(T newValue);

typedef SetCallback<T> = void Function(T newValue);
typedef TapCallback<T> = void Function(T newValue);
typedef SuffixCallback<T> = String Function(T value);
typedef GetCallback<T> = T Function();

typedef ChildHookCallback = Widget Function(Widget w);

mixin ComboInner {
  String getValueString();
}

abstract class AbstractPropertyWidget {
  bool customHeight = false;

  List<Widget> buildItems();
  bool isCentered() => false;

  void onTap(BuildContext context) { }
  void onLongPress(BuildContext context) { }

  Widget childHook(Widget w) {
    return w;
  }
}

Widget _buildBottomPicker(BuildContext context, Widget picker) {
  return Container(
    height: _kPickerSheetHeight,
    padding: const EdgeInsets.only(top: 6.0),
    color: CupertinoTheme.of(context).scaffoldBackgroundColor,
    child: DefaultTextStyle(
      style: TextStyle(
        color: CupertinoTheme.of(context).primaryColor,
        fontSize: 22.0,
      ),
      child: GestureDetector(
        // Blocks taps from propagating to the modal sheet and popping.
        onTap: () {},
        child: SafeArea(
          top: false,
          child: picker,
        ),
      ),
    ),
  );
}

class LinePropertyWidget extends AbstractPropertyWidget {
  final String line;
  final bool bold;
  final bool center;
  final bool right;
  final Color color;

  LinePropertyWidget.from(this.line, { this.bold = true, this.center = true, this.right = false, this.color });
  LinePropertyWidget({@required this.line, this.bold = true, this.center = true, this.right = false, this.color });

  @override
  bool isCentered() => true;

  @override
  List<Widget> buildItems() {
    return <Widget>[this.buildLine()];
  }

  Widget buildLine() {
    return Text(this.line,
      textAlign: this.center ? TextAlign.center : this.right ? TextAlign.right : TextAlign.left,
      style: TextStyle(
        color: this.color,
        fontWeight: this.bold ? FontWeight.bold : FontWeight.normal
      ));
  }
}

class ActionSheetPropertyWidget<Inner extends ComboInner> extends AbstractPropertyWidget {
  final String name;
  final String message;
  BuildContext context;
  List<Inner> objects;
  int currentObject;
  SetCallback<int> stateUpdate;

  ActionSheetPropertyWidget.from(this.context, this.name, this.objects, {
    this.currentObject = -1,
    this.stateUpdate,
    this.message,
  });

  @override
  List<Widget> buildItems() {
    return <Widget>[this.buildName(), this.buildValue()];
  }

  Widget buildName() => Text(this.name);
  Widget buildValue() {
    var text = (
      this.objects == null ||
      this.objects.length == 0 ||
      this.currentObject == -1 ||
      this.currentObject >= this.objects.length) ?
        "Tap to select" :
        this.objects[this.currentObject].getValueString();

    return Text(text, style: TextStyle(color: CupertinoColors.inactiveGray));
  }

  Widget _buildActionItem(int index) {
    var _button = CupertinoButton(
      color: CupertinoTheme.of(this.context).barBackgroundColor,
      child: Text(this.objects[index].getValueString(),
        style: TextStyle(color: CupertinoTheme.of(this.context).primaryColor)
      ),
      onPressed: () {
        this.stateUpdate(index);
        Navigator.pop(context);
      }
    );

    return _button;
  }

  Widget _buildActionSheet(BuildContext context) {
    var actions = List<Widget>.generate(this.objects.length, this._buildActionItem);
    return CupertinoActionSheet(
      message: this.message == null ? null : Text(this.message,
        style: TextStyle(color: CupertinoTheme.of(this.context).primaryColor)
      ),
      actions: actions,
      cancelButton: CupertinoButton(
        color: CupertinoTheme.of(this.context).barBackgroundColor,
        child: Text(i18n(context).globCancel,
          style: TextStyle(color: CupertinoTheme.of(this.context).primaryColor)
        ),
        onPressed: () {
          this.stateUpdate(-1);
          Navigator.pop(context);
        })
    );
  }

  @override onTap(BuildContext context) {
    showCupertinoModalPopup<void>(
      context: context,
      builder: this._buildActionSheet,
    );
  }
}

class ComboPropertyWidget<Inner extends ComboInner> extends AbstractPropertyWidget {
  final String name;
  final SetCallback<int> stateUpdate;
  List<Inner> objects;
  int currentObject;
  BuildContext context;

  ComboPropertyWidget.from(this.context, this.name, this.objects, {
    this.currentObject = -1,
    this.stateUpdate,
  });

  ComboPropertyWidget({
    @required this.name,
    @required this.objects,
    this.currentObject = -1,
    this.stateUpdate,
  });

  @override
  List<Widget> buildItems() {
    return <Widget>[this.buildName(), this.buildValue()];
  }

  Widget buildName() {
    return Text(this.name);
  }

  Widget buildValue() {
    var _text = (
      this.objects == null ||
      this.currentObject == -1 ||
      this.currentObject >= this.objects.length
    ) ? i18n(this.context).festNoPlayersAvailable : this.objects[this.currentObject].getValueString();

    return Text(_text,
      style: TextStyle(color: CupertinoColors.inactiveGray));
  }

  @override
  void onTap(BuildContext context) {
    final FixedExtentScrollController scrollController =
      FixedExtentScrollController(initialItem: this.currentObject == -1 ? 0 : this.currentObject);

    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return _buildBottomPicker(context,
          CupertinoPicker(
            scrollController: scrollController,
            itemExtent: _kPickerItemHeight,
            backgroundColor: null,// CupertinoTheme.of(context).scaffoldBackgroundColor,
            onSelectedItemChanged: (int index) {
              if(this.stateUpdate != null) {
                this.stateUpdate(index);
              }
            },
            children: List<Widget>.generate(this.objects.length, (int index) {
              return Center(child:
                Text(this.objects[index].getValueString()),
              );
            }),
          ),
        );
      },
    );
  }
}

class DatePropertyWidget extends AbstractPropertyWidget {
  final String name;
  final SetCallback<DateTime> setCallback;
  DateTime value;

  DatePropertyWidget.from(this.name, this.value, {this.setCallback});
  DatePropertyWidget({this.name, this.value, this.setCallback});

  @override
  List<Widget> buildItems() {
    return <Widget>[this.buildName(), this.buildValue()];
  }

  Widget buildName() {
    return Text(this.name);
  }

  Widget buildValue() {
    var _dateText = DateFormat.yMMMMd().format(this.value);
    return Text(_dateText,
      style: TextStyle(color: CupertinoColors.inactiveGray));
  }

  @override
  void onTap(BuildContext context) {
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return _buildBottomPicker(context,
          CupertinoDatePicker(
            mode: CupertinoDatePickerMode.date,
            initialDateTime: this.value,
            onDateTimeChanged: (DateTime newDateTime) {
              this.value = newDateTime;
              if(this.setCallback != null) {
                this.setCallback(this.value);
              }
            },
          ),
        );
      },
    );
  }
}

class BooleanPropertyWidget extends AbstractPropertyWidget {
  final String name;
  final Widget child;
  final SetCallback<bool> setCallback;
  final GetCallback<bool> getCallback;
  final bool defaultValue;

  BooleanPropertyWidget({this.name,
    this.child,
    this.defaultValue,
    this.getCallback,
    this.setCallback});

  @override
  List<Widget> buildItems() {
    return [this.buildName(), this.buildValue()];
  }

  Widget buildName() {
    if(this.child == null) {
      return Text(this.name);
    } else {
      return child;
    }
  }

  Widget buildValue() {
    return CupertinoSwitch(
      onChanged: (bool v) => this.setCallback(v),
      value: this.defaultValue,
    );
  }

  @override onTap(BuildContext ctx) {
    this.setCallback(!this.getCallback());
  }
}

class NumericPropertyWidget extends AbstractPropertyWidget {
  final NumberFormat _fmt = NumberFormat.decimalPattern();
  final String name;
  final SuffixCallback<int> suffix;
  final String editTitle;
  final String editBody;
  final CustomValidator<int> customValidator;
  final SetCallback<int> setCallback;
  bool erroneousValue = false;
  bool inactiveText = false;
  bool includeOriginalValueInSuffix = true;
  // This callback will be fired after widget creation. It gives the opportunity
  // to mutate the child after creation. The argument of the callback is the old
  // widget. Just returning this argument would create an identity transform
  // function, and it's equivalent to not specifying the 'mutateChild' callback
  // at all. Returning another widget will replace widget 'w' with the widget
  // that is returned by the callback.
  //
  // Normal use case for this callback would be to return i.e. Padding widget
  // which wraps the original widget.
  final ChildHookCallback mutateChild;
  final TapCallback<int> onTapped;
  final TapCallback<int> onLongPressed;
  int value;

  NumericPropertyWidget.from(this.name, this.value, {
    this.setCallback,
    this.suffix,
    this.editTitle,
    this.editBody,
    this.mutateChild,
    this.onTapped,
    this.onLongPressed,
    this.customValidator,
    this.erroneousValue,
    this.inactiveText,
    this.includeOriginalValueInSuffix,
  });

  NumericPropertyWidget({
    @required this.name,
    @required this.value,
    this.setCallback,
    this.suffix,
    this.editTitle,
    this.editBody,
    this.mutateChild,
    this.onTapped,
    this.onLongPressed,
    this.customValidator,
    this.erroneousValue,
    this.inactiveText,
    this.includeOriginalValueInSuffix,
  });

  @override
  Widget childHook(Widget w) {
    if(this.mutateChild != null) {
      return this.mutateChild(w);
    } else {
      return super.childHook(w);
    }
  }

  @override
  List<Widget> buildItems() {
    return [this.buildName(), this.buildValue()];
  }

  Widget buildName() {
    bool check = this.inactiveText == true;
    if(check) {
      return Text(this.name, style: TextStyle(color: CupertinoColors.inactiveGray));
    } else {
      return Text(this.name);
    }
  }

  String _formatValue() {
    if(this.value == null) {
      return "(null)";
    } else {
      return this._fmt.format(this.value);
    }
  }

  Widget buildValue() {
    var _numText = "";

    if(this.includeOriginalValueInSuffix != false)
      _numText += this._formatValue();

    if(this.suffix != null) {
      _numText += " ";
      _numText += this.suffix(this.value);
    }

    return Text(_numText,
      style: TextStyle(color:
        this.erroneousValue != null && this.erroneousValue == true? CupertinoColors.destructiveRed : CupertinoColors.inactiveGray));
  }

  @override
  void onLongPress(BuildContext context) {
    if(this.onLongPressed != null) {
      this.onLongPressed(this.value);
    }
  }

  @override
  void onTap(BuildContext context) {
    if(this.onTapped != null) {
      // User tap handler.
      this.onTapped(this.value);
    } else {
      // Default tap handler for Numeric type, but only when title/body are
      // specified. If not, ignore.

      if(this.editTitle == null || this.editBody == null)
        return;

      TextInputAlertDialog.show(context,
        title: this.editTitle ?? "",
        body: this.editBody ?? "",
        numericKeyboard: true,
        onOK: (buf) => this.setCallback(int.parse(buf)),
        textValidator: (buf) {
          var num = int.tryParse(buf);
          if(num == null)
            return false;

          if(this.customValidator != null) {
            return this.customValidator(num);
          } else {
            return true;
          }
        },
        initialText: "",
      );
    }
  }
}

class CustomPropertyWidget extends AbstractPropertyWidget {
  final Widget name;
  final Widget value;
  final TapCallback<void> onTapped;

  CustomPropertyWidget.from(this.name, this.value, {this.onTapped});
  CustomPropertyWidget({this.name, this.value, this.onTapped});

  @override
  List<Widget> buildItems() {
    return <Widget>[this.name, this.value];
  }

  @override
  void onTap(BuildContext context) {
    if(this.onTapped != null) {
      this.onTapped(null);
    }
  }
}

class IdentityPropertyWidget extends AbstractPropertyWidget {
  final Widget w;
  final bool center;

  IdentityPropertyWidget(this.w, {this.center = false}) {
    this.customHeight = true;
  }

  @override bool isCentered() => center;
  @override List<Widget> buildItems() => [w];
}

class TextPropertyWidget extends AbstractPropertyWidget {
  final String name;
  final String value;
  final TapCallback<String> onTapped;
  final bool boldName;
  final bool underlinedName;

  TextPropertyWidget.from(this.name, this.value, {
    this.onTapped,
    this.boldName = false,
    this.underlinedName = false,
  });

  TextPropertyWidget({
    @required this.name,
    @required this.value,
    this.onTapped,
    this.boldName = false,
    this.underlinedName = false,
  });

  @override
  List<Widget> buildItems() {
    return <Widget>[this.buildName(), this.buildValue()];
  }

  Widget buildName() {
    var weight = this.boldName ? FontWeight.bold : FontWeight.normal;
    var decor = this.underlinedName ? TextDecoration.underline : TextDecoration.none;

    return Text(this.name,
      style: TextStyle(
        fontWeight: weight, decoration: decor
      )
    );
  }

  Widget buildValue() {
    return Text(this.value,
      style: TextStyle(color: CupertinoColors.inactiveGray));
  }

  @override
  void onTap(BuildContext context) {
    if(this.onTapped != null) {
      // User tap handler.
      this.onTapped(this.value);
    }
  }
}

class PropertyListView extends StatefulWidget {
  final List<AbstractPropertyWidget> items;
  final bool drawLines;

  PropertyListView({Key key, this.items, this.drawLines = true}): super(key: key);

  @override
  _PropertyListViewState createState() => _PropertyListViewState();
}

class _PropertyListViewState extends State<PropertyListView> {
  Widget _buildRow(AbstractPropertyWidget item) {
    var _borderDecoration = this.widget.drawLines
      ? BoxDecoration(
          color: CupertinoTheme.of(context).scaffoldBackgroundColor,
          border: const Border(
            top: BorderSide(color: Color(0xFFBCBBC1), width: 0.0),
            bottom: BorderSide(color: Color(0xFFBCBBC1), width: 0.0),
          )
        )
      : BoxDecoration(
          color: CupertinoTheme.of(context).scaffoldBackgroundColor);

    var _container = Container(
      decoration: _borderDecoration,
      height: item.customHeight ? null : this.widget.drawLines ? 44.0 : 34.0,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: SafeArea(
          top: false,
          bottom: false,
          child: Row(
            mainAxisAlignment: item.isCentered() ?
              MainAxisAlignment.center :
              MainAxisAlignment.spaceBetween,
            children: item.buildItems(),
          ),
        ),
      ),
    );

    var _mutated = item.childHook(_container);
    return _mutated;
  }

  @override
  Widget build(BuildContext context) {
    var _lv = ListView(
      children: <Widget>[] + this.widget.items
        .where((AbstractPropertyWidget i) => i != null)
        .map((AbstractPropertyWidget item)
      {
        var _child = this._buildRow(item);
        var _detector = GestureDetector(
          child: _child,
          behavior: HitTestBehavior.opaque,
          onTap: () => item.onTap(context),
          onLongPress: () => item.onLongPress(context),
        );

        return _detector;
      }).toList(),
    );

    return _lv;
  }
}
